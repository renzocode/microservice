# microservice

# Interprocess communication in a microservice architecture.

Interprocess communication mechanisms (IPC)

synchronous
- REST API
- gRPC
asynchronous
- Messaging
- AMQP
- STOMP

It is important in a microservice architecture than a monolithic application.

messages formats JSON and XML.

there area a variety of client-service interaction style

- One to one (one service)
- one to many (multiple service)
- Synchronous (timely response)
- Asynchronous (is not necessarily sent immediately)

different types of one-to-many interactions:

- Publish/subscribe.
- Public/async responses.

It is important to know the API to use. ex Angular frontend - (REST and Websocket API) - java backend

[API-first design is essential](https://www.programmableweb.com/news/how-to-design-great-apis-api-first-design-and-raml/how-to/2015/07/10)

[USE SEMANTIC VERSIONING](http://semver.org)

TEXT - BASED MESSAGE FORMATS

[JSON](http://json-schema.org) and [XML](http://www.w3.org/XML/Schema)

BINARY MESSAGE FORMATS

[Protocol Buffers](https://developers.google.com/protocol-buffers/docs/overview)

[Avro](https://avro.apache.org)

[This blog post](http://martin.kleppmann.com/2012/12/05/schema-evolution-in-avro-protocol-buffers-thrift.html)

The Rest maturity model  
[REST](https://martinfowler.com/articles/richardsonMaturityModel.html)

[IDL for RESTful API](http://www.openapis.org)

[alternative API technologies such as GraphQL](http://graphql.org)

DEVELOPING ROBUST RPI PROXIES  
[Netflix Falcor](http://netflix.github.io/falcor/)

BROKER - BASED MESSAGING  
[ActiveMQ](http://activemq.apache.org)
[RabbitMQ](https://www.rabbitmq.com)
[Apache Kafka](http://kafka.apache.org)

Handling duplicate messages  
- WRITING IDEMPOTENT MESSAGE HANDLERS.
- TRACKING MESSAGES AND DISCARDING DUPLICATES.





















